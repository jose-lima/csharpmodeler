﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsParserApi.Tests
{
    [TestClass]
    public class CsParserTests
    {

        //TODO: 
        //- identify custom classes
        //- map classes relationships from code (text / UI)
        //- generate CS code from classes relationships map

        [TestMethod]
        public void ParseCustomTypeFields_onDifferentFiles_invertedOrder() {

            var text1 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class11 field2;\n\r"

                + "        public class Class11 { }\n\r"

                + "    }\n\r"
                + "}";

            var text2 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class3 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class11 field2;\n\r"
                + "        public Class1 field3;\n\r"

                + "    }\n\r"
                + "}";


            var project = new CsParser().ParseCsProject(text2, text1);
            var doc2 = project.CsDocuments[0];

            //Class
            Assert.AreEqual(1, doc2.Namespace.OwnClasses.Count(), "Unexpected number of classes.");
            var class3 = doc2.Namespace.OwnClasses[0];
            Assert.AreEqual("Class3", class3.ToString());

            //Fields
            Assert.AreEqual(3, class3.Fields.Count(), "Unexpected number of fields.");
            Assert.AreEqual("field1", class3.Fields[0].ToString());
            Assert.AreEqual("field2", class3.Fields[1].ToString());
            Assert.AreEqual("field3", class3.Fields[2].ToString());
        }

        [TestMethod]
        public void ParseCustomTypeFields_onDifferentFiles() {

            var text1 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class11 field2;\n\r"

                + "        public class Class11 { }\n\r"

                + "    }\n\r"
                + "}";

            var text2 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class3 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class11 field2;\n\r"

                + "    }\n\r"
                + "}";


            var project = new CsParser().ParseCsProject(text1, text2);
            var doc2 = project.CsDocuments[1];

            //Class
            Assert.AreEqual(1, doc2.Namespace.OwnClasses.Count(), "Unexpected number of classes.");
            var class3 = doc2.Namespace.OwnClasses[0];
            Assert.AreEqual("Class3",class3.ToString());

            //Fields
            Assert.AreEqual(2, class3.Fields.Count(), "Unexpected number of fields.");
            Assert.AreEqual("field2", class3.Fields[1].ToString());
        }

        [TestMethod]
        public void ParseCustomTypeFields_onSameFile() {

            var text =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class11 field2;\n\r"

                + "        public class Class11 { }\n\r"

                + "    }\n\r"
                + "}";


            var doc = new CsParser().ParseCsText(text);

            //Docs
            Assert.AreEqual(1, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual(2, doc.AllClasses.Count());

            //Classes
            var class1 = doc.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());
            Assert.AreEqual(class1, doc.AllClasses.ElementAt(0));

            var class11 = class1.OwnClasses[0];
            Assert.AreEqual("Class11", class11.ToString());
            Assert.AreEqual(class11, doc.AllClasses.ElementAt(1));

            //Fields
            Assert.AreEqual(2, class1.Fields.Count(), "Unexpected number of fields.");
            Assert.AreEqual("field1", class1.Fields[0].ToString());
            Assert.AreEqual("field2", class1.Fields[1].ToString(), "Unexpected name for custom type field.");
        }

        [TestMethod]
        public void ParseCsProject_fromMultipleStringVariables() {

            var text1 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class2 field2;\n\r"

                + "        public class Class11 { }\n\r"

                + "    }\n\r"
                + "}";

            var text2 =
                  "using System;\n\r"
                + "using System.Linq;\n\r"
                + "namespace SpaceName {\n\r"
                + "    public class Class2 {\n\r"
                + "    }\n\r"
                + "}";

            var project = new CsParser().ParseCsProject(text1, text2);

            //Docs
            Assert.AreEqual(2, project.CsDocuments.Count());
            var doc1 = project.CsDocuments.ElementAt(0);
            var doc2 = project.CsDocuments.ElementAt(1);
            Assert.AreEqual(1, doc1.Namespace.OwnClasses.Count());

            //Classes
            var class1 = doc1.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());

            var class11 = class1.OwnClasses[0];
            Assert.AreEqual("Class11", class11.ToString());

            var class2 = doc2.Namespace.OwnClasses[0];
            Assert.AreEqual("Class2", class2.ToString());
        }

        [TestMethod]
        public void ParseCsProject_fromExample() {

            var projectPath = @"C:\Users\user1\source\repos\CsParser4\ExampleLibrary";

            var csProject = new CsParser().ParseCsProject(projectPath);

            //Project
            Assert.AreEqual(3, csProject.CsDocuments.Count);

            //CsDocs
            var doc1 = csProject.CsDocuments.ElementAt(0);
            Assert.AreEqual(2, doc1.Namespace.OwnClasses.Count());

            var doc2 = csProject.CsDocuments.ElementAt(1);
            Assert.AreEqual(1, doc2.Namespace.OwnClasses.Count());

            var doc3 = csProject.CsDocuments.ElementAt(2);
            Assert.AreEqual(1, doc3.Namespace.OwnClasses.Count());

            //Classes
            var class1 = doc1.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());

            var class2 = doc1.Namespace.OwnClasses[1];
            Assert.AreEqual("Class2", class2.ToString());

            var class3 = doc2.Namespace.OwnClasses[0];
            Assert.AreEqual("Class3", class3.ToString());

            var class4 = doc3.Namespace.OwnClasses[0];
            Assert.AreEqual("Class4", class4.ToString());

            //Fields
            Assert.AreEqual(5, class1.Fields.Count(), $"Unexpected number of fields on class '{class1.ToString()}'.");
            Assert.AreEqual("field1", class1.Fields[0].ToString());
            Assert.AreEqual("field2", class1.Fields[1].ToString());
            Assert.AreEqual("field3", class1.Fields[2].ToString());
            Assert.AreEqual("field4", class1.Fields[3].ToString());
            Assert.AreEqual("field5", class1.Fields[4].ToString());

            Assert.AreEqual(0, class2.Fields.Count());

            Assert.AreEqual(0, class3.Fields.Count());

            Assert.AreEqual(2, class4.Fields.Count());
            Assert.AreEqual("field1", class4.Fields[0].ToString());
            Assert.AreEqual("field2", class4.Fields[1].ToString());

        }


        [TestMethod]
        public void GetAllClassesInDocument_independentOfDepth() {

            var text =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        private int field1;\n\r"
                + "        public Class2 field2;\n\r"

                + "        public class Class11 { }\n\r"

                + "    }\n\r"
                + "}";


            var doc = new CsParser().ParseCsText(text);
            var allDocClasses = doc.AllClasses;

            //Docs
            Assert.AreEqual(1, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual(2, allDocClasses.Count());

            //Classes9
            var class1 = doc.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());
            Assert.AreEqual(class1, allDocClasses.ElementAt(0));

            var class11 = class1.OwnClasses[0];
            Assert.AreEqual("Class11", class11.ToString());
            Assert.AreEqual(class11, allDocClasses.ElementAt(1));

        }

        [TestMethod]
        public void ParseFieldsNames_multiple() {

            var text =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"

                + "        int field1;\n\r"
                + "        private string field2;\n\r"

                + "    }\n\r"

                + "}";

            var doc = new CsParser().ParseCsText(text);

            var class1 = doc.Namespace.OwnClasses[0];
            Assert.AreEqual(2, class1.Fields.Count());
            Assert.AreEqual("field1", class1.Fields[0].ToString());
            Assert.AreEqual("field2", class1.Fields[1].ToString());

        }

        [TestMethod]
        public void ParseClassesNames_multiple_nested() {

            var text =
                  "using System;\n\r"
                + "using System.Linq;\n\r"

                + "namespace SpaceName {\n\r"

                + "    public class Class1 {\n\r"
                + "        public class Class11 { }\n\r"
                + "    }\n\r"

                + "    public class Class2 {\n\r"
                + "        public class Class21 { }\n\r"
                + "        public class Class22 { }\n\r"
                + "    }\n\r"

                + "}";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(2, doc.Namespace.OwnClasses.Count());


            //Top classes
            var class1 = doc.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());

            var class2 = doc.Namespace.OwnClasses[1];
            Assert.AreEqual("Class2", class2.ToString());


            //Sub classes
            var class11 = class1.OwnClasses[0];
            Assert.AreEqual("Class11", class11.ToString());

            var class21 = class2.OwnClasses[0];
            Assert.AreEqual("Class21", class21.ToString());

            var class22 = class2.OwnClasses[1];
            Assert.AreEqual("Class22", class22.ToString());
        }

        [TestMethod]
        public void ParseClassesNames_nested() {

            var text =
                  "using System;\n\r"
                + "using System.Linq;\n\r"
                + "namespace SpaceName {\n\r"
                + "    public class Class1 {\n\r"
                + "        public class Class11 { }\n\r"
                + "    }\n\r"
                + "}";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(1, doc.Namespace.OwnClasses.Count());

            var class1 = doc.Namespace.OwnClasses[0];
            Assert.AreEqual("Class1", class1.ToString());

            var class11 = class1.OwnClasses[0];
            Assert.AreEqual("Class11", class11.ToString());
        }


        [TestMethod]
        public void ParseClassesNames_multiple() {

            var text = "using System; using System.Linq; namespace SpaceName { public class Class1 { } public class Class2 { } }";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(2, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual("Class1", doc.Namespace.OwnClasses[0].ToString());
            Assert.AreEqual("Class2", doc.Namespace.OwnClasses[1].ToString());
        }

        [TestMethod]
        public void ParseClassesNames_multiple_newLines() {

            var text = "using System;\n\r"
                + "using System.Linq;\n\r"
                + "namespace SpaceName\n\r"
                + "{\n\r"
                + "    public class Class1 { }\n\r"
                + "    public class Class2 { }\n\r"
                + "}";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(2, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual("Class1", doc.Namespace.OwnClasses[0].ToString());
            Assert.AreEqual("Class2", doc.Namespace.OwnClasses[1].ToString());
        }

        [TestMethod]
        public void ParseClassesNames_single_newLines() {

            var text = "using System;\n\r"
                + "using System.Linq;\n\r"
                + "namespace SpaceName\n\r"
                + "{\n\r"
                + "    public class Class1 { }\n\r"
                + "}";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(1, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual("Class1", doc.Namespace.OwnClasses[0].ToString());
        }

        [TestMethod]
        public void ParseClassesNames_single() {

            var text = "using System; using System.Linq; namespace SpaceName { public class Class1 { } }";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual(1, doc.Namespace.OwnClasses.Count());
            Assert.AreEqual("Class1", doc.Namespace.OwnClasses[0].ToString());
        }


        [TestMethod]
        public void ParseNamespaceName() {

            var text = "using System; using System.Linq; namespace SpaceName { public class Class1 { } }";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual("SpaceName", doc.Namespace.ToString());

        }


        [TestMethod]
        public void ParseUsings() {

            var text = "using System; using System.Linq; namespace SpaceName { public class Class1 { } }";

            var doc = new CsParser().ParseCsText(text);

            Assert.AreEqual("System", doc.DllReferences[0].ToString());
            Assert.AreEqual("System.Linq", doc.DllReferences[1].ToString());
        }
    }
}
