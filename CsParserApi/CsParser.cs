﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace CsParserApi {


    public class CsParser {

        private StringBuilder currentToken = new StringBuilder();
        private CsDocument csDocument;
        private Stack<CsParadigma> tempTokens = new Stack<CsParadigma>();
        private CsScope currentScope;
        private List<CsScope> unparsedScopes = new List<CsScope>();
        private static readonly string[] ExcludedFolders = new string[] { "bin", "obj", "Properties" };
        private CsSyntax csSyntax;


        public CsParser() {
            csSyntax = new CsSyntax();
        }

        public CsProject ParseCsProject(string projectPath) {

            //Get CS files contents
            var csFilesTexts = GetCsFilesContents(projectPath);

            //Parse files
            return ParseCsProject(csFilesTexts.ToArray());
        }

        private static IEnumerable<string> GetCsFilesContents(string projectPath) {
            var csFilePaths = Directory.GetFiles(projectPath, "*.cs", SearchOption.AllDirectories);
            var filteredCsFilePaths = csFilePaths.Where(x => !ExcludedFolders.Any(y => x.Contains(y)));
            var csFilesTexts = new List<string>();
            foreach (var csFilePath in filteredCsFilePaths) {
                var csFileText = File.ReadAllText(csFilePath);
                csFilesTexts.Add(csFileText);
            }
            return csFilesTexts;
        }

        public CsProject ParseCsProject(params string[] csFilesTexts) {

            var csProject = new CsProject();
            foreach (var csFileText in csFilesTexts) {
                var csDoc = ParseCsText(csFileText);
                csProject.Add(csDoc);
            }

            return csProject;
        }

        public CsDocument ParseCsFile(string filePath) {
            var text = File.ReadAllText(filePath);
            return ParseCsText(text);
        }

        public CsDocument ParseCsText(string text) {
            csDocument = new CsDocument();
            currentScope = csDocument;
            Iterate(text);
            return csDocument;
        }


        private void Iterate(string text) {

            for (int i = 0; i < text.Length; i++) {
                ParseChar(text[i]);
            }

            ParseCustomTypes(csDocument);
        }


        private void ParseChar(char c) {

            //Add token to document & restart
            if (CsSyntax.IsTokenSeparator(c)) {


                if (currentToken.Length > 0) {
                    var newParadigma = csSyntax.GetCsMeaning(currentToken.ToString());
                    Add(csDocument, newParadigma);
                }

                if (CsSyntax.IsSeparator(c)) {
                    var newParadigma = CsSeparator.Create(c);
                    Add(csDocument, newParadigma);
                }

                currentToken.Clear();
                return;
            }

            //Add char to current token
            if (!CsSyntax.IsSilenceChar(c)) {
                currentToken.Append(c);
            }
        }


        private void Add(CsDocument csDocument, CsParadigma csParadigma) {
            tempTokens.Push(csParadigma);
            RearrangeTokens();
        }


        private void RearrangeTokens() {

            var separator = tempTokens.First() as CsSeparator;
            if (separator == null) {
                return;
            }
            tempTokens.Pop();


            if (separator is CsScopeSeparator) {

                //Usings
                if (CsGrammar.IsDllSignature(tempTokens)) {
                    currentScope.Add(new CsDllReference(tempTokens.ElementAt(0).RawText));
                }

                //Field
                else if (CsGrammar.IsFieldSignature(tempTokens)) {
                    currentScope.Add(new CsField(tempTokens.ElementAt(0).RawText));

                }

                //Unparsable scope
                else {
                    var unparsedScope = new CsUnparsedScope(tempTokens.ToArray());
                    currentScope.Add(unparsedScope);
                    unparsedScopes.Add(unparsedScope);
                }

                tempTokens.Clear();

            }


            else if (separator is CsScopeOpener) {

                CsScope newScope = null;

                //Namespace
                if (CsGrammar.IsNamespaceSignature(tempTokens)) {
                    if (csDocument.Namespace != null) {
                        throw new ArgumentException("A CS file can only have 1 namespace definition.");
                    }
                    newScope = new CsNamespace(tempTokens.ElementAt(0).RawText);
                    currentScope.Add(newScope);
                    currentScope = newScope;
                }

                //Class
                else if (CsGrammar.IsClassSignature(tempTokens)) {
                    newScope = new CsClass(tempTokens.ElementAt(0).RawText);
                    csSyntax.AddUserType(newScope.ToString());
                    currentScope.Add(newScope);
                    currentScope = newScope;
                }

                //Unparsable scope
                else {
                    newScope = new CsUnparsedScope(tempTokens.ToArray());
                    unparsedScopes.Add(newScope);
                    currentScope.Add(newScope);
                }

                tempTokens.Clear();
            }


            else if (separator is CsScopeCloser) {

                currentScope = currentScope.ParentScope;
                tempTokens.Clear();
            }

        }

        internal void ParseCustomTypes(CsDocument csDocument) {

            var unparsedScopesCopy = unparsedScopes.ToArray();
            for (var i=0; i< unparsedScopes.Count(); i++) {

                var unparsedScope = unparsedScopesCopy[i];

                //Reparse tokens
                var newTokensList = new Stack<CsParadigma>();
                foreach(var originalToken in unparsedScopesCopy[i].InnerScopes) {
                    var newParadigma = originalToken;
                    if (originalToken is CsString) {
                        newParadigma = csSyntax.GetCsMeaning(originalToken.ToString());         
                    }
                    newTokensList.Push(newParadigma);
                }

                //Reparse token sequence
                var tempTokens = newTokensList.Reverse();
                if (CsGrammar.IsFieldSignature(tempTokens)) {
                    unparsedScopes.Remove(unparsedScope);
                    unparsedScope.Replace(new CsField(tempTokens.ElementAt(0).RawText));
                }
            }
        }
    }
}

