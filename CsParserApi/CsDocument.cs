﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CsParserApi {

    public class CsDocument: CsScope {


        public CsDllReference[] DllReferences => innerScopes.OfType<CsDllReference>().ToArray();

        public CsNamespace Namespace => innerScopes.OfType<CsNamespace>().SingleOrDefault();

    }
}
