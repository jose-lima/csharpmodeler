﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsParserApi {

    public class CsSyntax {

        public static readonly char[] SilentChars = new char[] { ' ', '\n', '\r' };

        public static readonly char ScopeSeparator = ';';

        public static readonly char ScopeOpenSeparator = '{';

        public static readonly char ScopeCloserSeparator = '}';

        public static readonly string[] AccessorKeys = new string[] {
            "public",
            "private",
            "internal",
            "protected"
        };

        public static readonly string[] ScopeKeys = new string[] {
            "using",
            "namespace",
            "class"
        };

        public static readonly string[] TypeKeys = new string[] {
            "int",
            "double",
            "string"
        };

        public List<string> CustomTypeKeys = new List<string>();

        public static readonly string[] ModifierKeys = new string[] {
            "const",
            "readonly",
            "string"
        };

        public static readonly char[] Separators = new[] { ScopeSeparator, ScopeOpenSeparator, ScopeCloserSeparator };

        public static readonly char[] TokenSeparators = SilentChars.Concat(Separators).ToArray();

        public IEnumerable<string> CsKeys =>
            ScopeKeys
            .Concat(AccessorKeys)
            .Concat(TypeKeys)
            .Concat(ModifierKeys)
            .Concat(CustomTypeKeys);


        public void AddUserType(string typeName) {
            if(CustomTypeKeys.Any(x => x.Equals(typeName))) {
                return;
            }
            CustomTypeKeys.Add(typeName);
        }

        public static bool IsSeparator(char c) {
            return Separators.Contains(c);
        }

        public static bool IsScopeSeparator(char c) {
            return c.Equals(ScopeSeparator);
        }

        public static bool IsScopeOpener(char c) {
            return c.Equals(ScopeOpenSeparator);
        }

        public static bool IsScopeCloser(char c) {
            return c.Equals(ScopeCloserSeparator);
        }

        public static bool IsTokenSeparator(char c) {
            return TokenSeparators.Contains(c);
        }

        public static bool IsCsAccessor(string word) {
            return AccessorKeys.Contains(word);
        }

        public static bool IsSilenceChar(char c) {
            return SilentChars.Contains(c);
        }

        public static bool IsCsScopeKey(string word) {
            return ScopeKeys.Contains(word);
        }

        public bool IsCsKey(string word) {
            return CsKeys.Contains(word);
        }

        public CsParadigma GetCsMeaning(string word) {

            if (IsCsKey(word)) {
                return CreateCsKey(word);
            }

            return new CsString(word);
        }

        public CsKey CreateCsKey(string word) {
            if (word.Equals("using")) {
                return new CsUsingsKey();
            }

            else if (word.Equals("namespace")) {
                return new CsNamespaceKey();
            }

            else if (word.Equals("class")) {
                return new CsClassKey();
            }

            else if (IsCsAccessor(word)) {
                return new CsAccessorKey(word);
            }

            else if (TypeKeys.Contains(word)) {
                return new CsTypeKey(word);
            }

            else if (CustomTypeKeys.Contains(word)) {
                return new CsCustomTypeKey(word);
            }

            throw new ArgumentException($"'{word}' is not a known CS language key.");
        }
    }

}
