﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsParserApi {

    public class CsNamespace : CsScope {

        public CsNamespace(string word): base(word) { }

    }

    public class CsUnparsedScope : CsScope {

        public CsUnparsedScope(params CsParadigma[] innerScopes) {
            AddRange(innerScopes.ToArray());
        }

        public override string ToString() {
            return string.Join(" ", innerScopes.ToString());
        }
    }

    public class CsClass : CsScope {

        public CsKey CsAccess { get; set; }

        public CsField[] Fields => InnerScopes.OfType<CsField>().ToArray();

        public CsClass(string className): base(className) {
        }
    }

    public class CsField : CsScope {

        public CsKey CsAccess { get; set; }

        public CsField(string text) : base(text) {
        }

    }

    public class CsScope : CsParadigma {

        protected List<CsParadigma> innerScopes = new List<CsParadigma>();

        public CsClass[] OwnClasses => InnerScopes.OfType<CsClass>().ToArray();

        public IEnumerable<CsClass> AllClasses => 
            innerScopes.OfType<CsClass>().Concat(innerScopes.OfType<CsScope>().SelectMany(x => x.AllClasses));

        protected CsScope() {
        }

        public CsScope(string word): base(word) {
        }

        public IReadOnlyCollection<CsParadigma> InnerScopes => innerScopes.AsReadOnly();

        public virtual void Add(CsParadigma innerScope) {
            innerScope.ParentScope = this;
            innerScopes.Add(innerScope);
        }

        public virtual void Remove(CsParadigma scopeToRemove) {
            innerScopes.Remove(scopeToRemove);
        }

        public virtual void AddRange(params CsParadigma[] innerScopes) {
            foreach (var innerScope in innerScopes) {
                Add(innerScope);
            }
        }

        internal void Replace(CsScope newScope) {
            newScope.ParentScope = ParentScope;
            ParentScope.Remove(this);
            ParentScope.Add(newScope);
        }

    }

    public class CsDllReference : CsScope {
        public CsDllReference(string text) : base(text) {
        }
    }

    public class CsString : CsScope {
        public CsString(string text):base(text) {
        }
    }

    public class CsScopeSeparator: CsSeparator {

    }

    public class CsScopeOpener : CsSeparator {

    }

    public class CsScopeCloser : CsSeparator {

    }

    public class CsSeparator : CsParadigma {

        internal CsSeparator() {}

        public static CsSeparator Create(char separatorText) {

            if (!CsSyntax.Separators.Contains(separatorText)) {
                throw new ArgumentException($"'{separatorText}' is not a valid separator character.");
            }

            CsSeparator separator = null;
            if (CsSyntax.IsScopeSeparator(separatorText)) {
                separator = new CsScopeSeparator();
            }

            else if (CsSyntax.IsScopeOpener(separatorText)) {
                separator = new CsScopeOpener();
            }

            else if (CsSyntax.IsScopeCloser(separatorText)) {
                separator = new CsScopeCloser();
            }

            separator.RawText = separatorText.ToString();

            return separator;
        }
    }

    public class CsNamespaceKey : CsKey {
        public CsNamespaceKey() {
        }

        public CsNamespaceKey(string word) : base(word) {
        }
    }

    public class CsClassKey : CsKey {

        public CsClassKey() {
        }

        public CsClassKey(string word) : base(word) {
        }
    }

    public class CsUsingsKey : CsKey {
        public CsUsingsKey() {
        }

        public CsUsingsKey(string word) : base(word) {
        }
    }

    public class CsAccessorKey : CsKey {

        public CsAccessorKey(string word): base(word) {
        }
    }

    public class CsTypeKey : CsKey {
        public CsTypeKey(string word) : base(word) {
        }
    }

    public class CsCustomTypeKey : CsTypeKey {
        public CsCustomTypeKey(string word) : base(word) {
        }
    }

    public class CsKey : CsParadigma {

        protected CsKey() {
        }

        protected CsKey(string word) : base(word) {
        }

    }

    public class CsParadigma {

        public CsScope ParentScope { get; set; }

        public string RawText { get; set; }

        protected CsParadigma() { }

        public CsParadigma(string text) {
            RawText = text;
        }

        public override string ToString() {
            return RawText;
        }

        public override bool Equals(object obj) {

            //reference
            if (ReferenceEquals(this, obj)) {
                return true;
            }

            //type
            if (!GetType().Equals(obj.GetType())) {
                return false;
            }

            //content
            var casted = obj as CsParadigma;
            if (casted == null) {
                return false;
            }

            return Equals(casted);
        }

        public bool Equals(CsParadigma obj) {
            return RawText == obj.RawText;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
    }
}
