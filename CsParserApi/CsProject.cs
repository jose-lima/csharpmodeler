﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsParserApi {

    public class CsProject {

        public List<CsDocument> CsDocuments { get; } = new List<CsDocument>();

        public void Add(CsDocument csDocument) {
            CsDocuments.Add(csDocument);
        }
    }
}
