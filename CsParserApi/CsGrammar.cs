﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsParserApi {

    public class CsGrammar {

        /// <summary>
        /// Example: "using DllName;"
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        internal static bool IsDllSignature(Stack<CsParadigma> tokens) {

            return 
                tokens.Count == 2
                && tokens.ElementAt(0) is CsString
                && tokens.ElementAt(1) is CsUsingsKey
                ;
        }


        /// <summary>
        /// Example: "namespace SpaceName {"
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        internal static bool IsNamespaceSignature(Stack<CsParadigma> tokens) {

            return
                tokens.Count == 2
                && tokens.ElementAt(0) is CsString
                && tokens.ElementAt(1) is CsNamespaceKey
                ;
        }


        /// <summary>
        /// Example: "public class ClassName {"
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        internal static bool IsClassSignature(Stack<CsParadigma> tokens) {

            //Without accessor
            if (tokens.Count == 2) {
                return
                    tokens.ElementAt(0) is CsString
                    && tokens.ElementAt(1) is CsClassKey;
            }

            //With accessor
            else if (tokens.Count == 3) {
                return
                    tokens.ElementAt(0) is CsString
                    && tokens.ElementAt(1) is CsClassKey
                    && tokens.ElementAt(2) is CsAccessorKey;
            }

            return false;
        }


        /// <summary>
        /// Example: "private const int field1 = 5;"
        /// </summary>
        /// <param name="tokens"></param>
        /// <returns></returns>
        internal static bool IsFieldSignature(IEnumerable<CsParadigma> tokens) {

            //Unassigned cases:
            //Case: "int field1;"
            //Case: "private int field1;"
            //Case: "private const int field1;"

            //Assigned cases:
            //Case: "int field1 = 5;"
            //Case: "private int field1 = 5;"
            //Case: "private const int field1 = 5;"

            //Case: "int field1;"
            if (tokens.Count() == 2) {
                return
                    tokens.ElementAt(0) is CsString
                    && tokens.ElementAt(1) is CsTypeKey;
            }

            //Case: "private int field1;"
            else if (tokens.Count() == 3) {
                return
                    tokens.ElementAt(0) is CsString
                    && tokens.ElementAt(1) is CsTypeKey
                    && tokens.ElementAt(2) is CsAccessorKey;
            }

            //TODO

            return false;
        }

    }
}
