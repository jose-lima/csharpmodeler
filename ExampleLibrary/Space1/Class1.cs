﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleLibrary.Space2;

namespace ExampleLibrary.Space1
{
    public class Class1
    {
        int field1;
        private string field2;
        public Class2 field3;
        public Class3 field4;
        public Class4 field5;

        public class Class11 {
            private string field1;
        }
    }

    public class Class2 {
        protected double field1 = 1.5;
    }
}
